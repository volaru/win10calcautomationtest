﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Remote;


namespace CalcTestExample
{
    [TestClass]
    public class CalculatorTesting
    {
        private const string WindowsApplicationDriverUrl = "http://127.0.0.1:4723/wd/hub";
        private const string CalculatorAppId = "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App";

        protected static WindowsDriver<WindowsElement> session;

        public TestContext TestContext { get; set; }



        [TestMethod]
        public void StartNewCalcSession()
        {

            if (session == null)
            {

                DesiredCapabilities appCapabilities = new DesiredCapabilities();
                appCapabilities.SetCapability("app", CalculatorAppId);
                appCapabilities.SetCapability("deviceName", "Windows PC");
                appCapabilities.SetCapability("platformName", "Windows");

                var startTime = DateTime.Now;
                session = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), appCapabilities);
                var startupTime = (DateTime.Now - startTime).TotalMilliseconds;


                 
                Assert.IsNotNull(session);
                Console.WriteLine("Session Started Succesfully. Calc is running!");

                Console.WriteLine($"App started in {startupTime} seconds");
                Assert.IsTrue(startupTime < 4000);


            }

        }

        [TestMethod]
        public void TwoNumberAdditionTest()
        {
            var number1 = "2";
            var number2 = "3";
            var expectedResult = "5";

            ValuesUsed(number1, number2, expectedResult);

            typeNumber(number1);
            session.FindElementByAccessibilityId("plusButton").Click();
            typeNumber(number2);
            session.FindElementByAccessibilityId("equalButton").Click();

            Assert.AreEqual(expectedResult, GetCalculatorResultText());
        }

        [TestMethod]
        public void TwoNumberSubtractionTest()
        {
            var number1 = "2";
            var number2 = "3";
            var expectedResult = "-1";

            ValuesUsed(number1, number2, expectedResult);

            typeNumber(number1);
            session.FindElementByAccessibilityId("minusButton").Click();
            typeNumber(number2);
            session.FindElementByAccessibilityId("equalButton").Click();

            Assert.AreEqual(expectedResult, GetCalculatorResultText());
        }

        [TestMethod]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
                    "|DataDirectory|\\divisionData.csv", "divisionData#csv", DataAccessMethod.Sequential),
                    DeploymentItem("divisionData.csv")]

        public void TwoNumberDivisionTest()
        {
            string number1 = TestContext.DataRow["Number1"].ToString();
            string number2 = TestContext.DataRow["Number2"].ToString();
            string resultType = TestContext.DataRow["resultType"].ToString();
            string expectedResult;


            if (resultType == "number")
            {
                expectedResult = TestContext.DataRow["NumberExpectedResult"].ToString();
            }
            else
            {
                expectedResult = TestContext.DataRow["StringExpectedResult"].ToString();
            }

            ValuesUsed(number1, number2, expectedResult);

            typeNumber(number1);
            session.FindElementByAccessibilityId("divideButton").Click();
            typeNumber(number2);
            session.FindElementByAccessibilityId("equalButton").Click();

            Assert.AreEqual(expectedResult, GetCalculatorResultText());

        }

        [TestMethod]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
                    "|DataDirectory|\\multiplicationData.csv", "multiplicationData#csv", DataAccessMethod.Sequential),
                    DeploymentItem("multiplicationData.csv")]

        public void TwoNumberMultiplicationTest()
        {
            string number1 = TestContext.DataRow["Number1"].ToString();
            string number2 = TestContext.DataRow["Number2"].ToString();
            string expectedResult = TestContext.DataRow["ExpectedResult"].ToString();

            ValuesUsed(number1, number2, expectedResult);

            typeNumber(number1);
            session.FindElementByAccessibilityId("multiplyButton").Click();
            typeNumber(number2);
            session.FindElementByAccessibilityId("equalButton").Click();

            Assert.AreEqual(expectedResult, GetCalculatorResultText());

        }

        [TestMethod]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
                    "|DataDirectory|\\deletenumberData.csv", "deletenumberData#csv", DataAccessMethod.Sequential),
                    DeploymentItem("deletenumberData.csv")]

        public void DeleteWrongNumberTest()
        {
            string number = TestContext.DataRow["Number"].ToString();
            string ButtonName = TestContext.DataRow["ButtonName"].ToString();
            string resultType = TestContext.DataRow["resultType"].ToString();
            string expectedResult;


            if (resultType == "number")
            {
                expectedResult = TestContext.DataRow["NumberExpectedResult"].ToString();
            }
            else
            {
                expectedResult = TestContext.DataRow["StringExpectedResult"].ToString();
            }

            ValuesUsed(number, ButtonName, expectedResult);

            typeNumber(number);
            session.FindElementByAccessibilityId(ButtonName).Click();

            Assert.AreEqual(expectedResult, GetCalculatorResultText());

            session.FindElementByAccessibilityId("clearEntryButton").Click();

        }


        [TestMethod]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
                    "|DataDirectory|\\memorytesDatat.csv", "memorytesDatat#csv", DataAccessMethod.Sequential),
                    DeploymentItem("memorytesDatat.csv")]

        public void NumbersInMemoryTest()
        {
            double previuousvalueinmemory = 0;
            double testMathExpected = 0;
            string number = TestContext.DataRow["Number"].ToString();
            string ButtonName = TestContext.DataRow["ButtonName"].ToString();

            //expectedResult not used because i calculate the expected in this test method.
            //Can be uncommented to double check the testMathExpected values
            //string expectedResult = TestContext.DataRow["ExpectedResult"].ToString(); 

            session.FindElementByAccessibilityId("clearEntryButton").Click();
            //session.FindElementByAccessibilityId("MemoryLabel").Click();
                    

            if (session.FindElementByAccessibilityId("MemRecall").Enabled)
            {
                session.FindElementByAccessibilityId("MemRecall").Click();
                previuousvalueinmemory = double.Parse(GetCalculatorResultText(), CultureInfo.InvariantCulture.NumberFormat);

            }


            typeNumber(number);

            if (ButtonName == "memButton")
            {
                testMathExpected = previuousvalueinmemory + double.Parse(GetCalculatorResultText(), CultureInfo.InvariantCulture.NumberFormat);
                session.FindElementByAccessibilityId(ButtonName).Click();
            }
            if (ButtonName == "MemPlus")
            {
                testMathExpected = previuousvalueinmemory + double.Parse(GetCalculatorResultText(), CultureInfo.InvariantCulture.NumberFormat);
                session.FindElementByAccessibilityId(ButtonName).Click();
            }
            if (ButtonName == "MemMinus")
            {
                testMathExpected = previuousvalueinmemory - double.Parse(GetCalculatorResultText(), CultureInfo.InvariantCulture.NumberFormat);
                session.FindElementByAccessibilityId(ButtonName).Click();
            }

            session.FindElementByAccessibilityId("clearEntryButton").Click();

            ValuesUsed(number, ButtonName, testMathExpected.ToString());
            session.FindElementByAccessibilityId("MemRecall").Click();


            Assert.AreEqual(testMathExpected.ToString(), GetCalculatorResultText());
            //Assert.AreEqual(expectedResult, testMathExpected.ToString());

        }


        [TestMethod]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "|DataDirectory|\\resetcalculatorData.csv", "resetcalculatorData#csv", DataAccessMethod.Sequential),
            DeploymentItem("resetcalculatorData.csv")]

        public void ResetCalculatorTest()
        {
            string ButtonName = TestContext.DataRow["ButtonName"].ToString();
            string ButtonNameSecondary = TestContext.DataRow["ButtonNameSecondary"].ToString();
            string FindIfEnabled = TestContext.DataRow["FindIfEnabled"].ToString();
            string ResetCalcAppArea = TestContext.DataRow["ResetCalcAppArea"].ToString();
            string ExpectedResult = TestContext.DataRow["ExpectedResult"].ToString();

            session.FindElementByAccessibilityId("CalculatorResults").Click();

            if (IsElementPresent(ButtonName))
            {
                session.FindElementByAccessibilityId(ButtonName).Click();


                if(ButtonName == "clearButton")
                {
                    // provide test data for clearButton
                    typeNumber("1");
                    session.FindElementByAccessibilityId("plusButton").Click();
                    typeNumber("2");
                    session.FindElementByAccessibilityId("minusButton").Click();
                    typeNumber("3");
                }

                Console.WriteLine("Button Name found = " + ButtonName);

            }
            else
            {
                ButtonName = ButtonNameSecondary;
                session.FindElementByAccessibilityId(ButtonName).Click();
                Console.WriteLine("Button Name found = " + ButtonName);

            }

            if (IsElementEnabled(FindIfEnabled))
            {
                Console.WriteLine("Element Found = " + FindIfEnabled);
                session.FindElementByAccessibilityId(ResetCalcAppArea).Click();
                Console.WriteLine("Clicked on button = " + ResetCalcAppArea);

            }
            else
            {
                Console.WriteLine("Element Missing = " + FindIfEnabled);
                Assert.Inconclusive(" - Element is missing - provide test data in order to continue");
            }


            if(ButtonName != "clearButton")
            {
                Console.WriteLine("aaaaData was cleared from " + FindIfEnabled + " and now displaying " + ExpectedResult);
                Assert.IsTrue(IsElementEnabled(ExpectedResult));
            }
            else
            {
                Console.WriteLine("Data was cleared from " + FindIfEnabled + " and now " + ExpectedResult + " is Disabled");
                Assert.IsFalse(IsElementEnabled(ExpectedResult));
            }

        }

        [TestMethod]

        public void OperationsInHistoryTest()
        {
            Random random = new Random();
            List<string> operation = new List<string> { "plusButton", "minusButton", "multiplyButton" , "divideButton" };
            decimal testMath;
            string ExpectedResult = "";


            foreach (string item in operation)
            {
                var number1 = random.Next(-10, 10);
                var number2 = random.Next(-10, 10);

                if (item == "divideButton") //avoid division by 0
                {
                    while (number2 == 0)
                    {
                        number2 = random.Next(-10, 10);
                    }
                }
                                            
                typeNumber(number1.ToString());
                session.FindElementByAccessibilityId(item).Click();
                typeNumber(number2.ToString());
                session.FindElementByAccessibilityId("equalButton").Click();

                
                if(item == "plusButton")
                {
                    testMath = number1 + number2;
                    ExpectedResult = testMath.ToString();
                    Console.WriteLine($"Test Math result: {testMath}");

                }
                else if(item == "minusButton")
                {
                    testMath = number1 - number2;
                    ExpectedResult = testMath.ToString();
                    Console.WriteLine($"Test Math result: {testMath}");

                }
                else if (item == "multiplyButton")
                {
                    testMath = number1 * number2;
                    ExpectedResult = testMath.ToString();
                    Console.WriteLine($"Test Math result: {testMath}");

                }
                else if (item == "divideButton")
                {
                    testMath = Convert.ToDecimal(number1) / Convert.ToDecimal(number2);

                    if(testMath <0)
                    {
                        ExpectedResult = testMath.ToString("0.###############");
                        Console.WriteLine($"Test Math result: {ExpectedResult}");

                    }
                    else
                    {
                        ExpectedResult = testMath.ToString("0.###############");
                        Console.WriteLine($"Test Math result: {ExpectedResult}");                
                    }

                }

                var ActualResult = session.FindElementByAccessibilityId("resultTextBlock").Text;
                Console.WriteLine("Value in resultTextBlock from History List is: " + ActualResult);

                Assert.AreEqual(ExpectedResult, ActualResult);
                Console.WriteLine("Expected matches Actual. Test Pass");

            }

        }

        [TestMethod]
        public void EndNewCalcSession()
        {
            FinishTests();
        }






        private static void ValuesUsed(string number1, string number2, string expectedResult)
        {
            Console.WriteLine("number1=" + number1);
            Console.WriteLine("number2=" + number2);
            Console.WriteLine("expectedResult=" + expectedResult);
        }

        private string GetCalculatorResultText()
        {
            return session.FindElementByAccessibilityId("CalculatorResults").Text.Replace("Display is", string.Empty).Trim();
        }

        private string GetLastDigitCalculatoResults()
        {
            var displayedNuber = session.FindElementByAccessibilityId("CalculatorResults").Text.Replace("Display is", string.Empty).Trim();
            // Console.WriteLine("ActualResult=" + displayedNuber);

            if (displayedNuber.Length == 1)
            {
                return displayedNuber;
            }
            else
            {
                return displayedNuber.Substring(displayedNuber.Length - 1);
            }
        }

        public static void typeNumber(string numberData)
        {
            var negativeNumber = false;

            if (numberData[0] == '-')
            {
                negativeNumber = true;
                numberData = numberData.Substring(1);
            }


            var numberLenght = numberData.Length;

            while (numberLenght > 0)
            {
                if (numberData[0] != '.')
                {
                    session.FindElementByAccessibilityId("num" + numberData[0] + "Button").Click();
                    numberData = numberData.Substring(1);
                    numberLenght -= 1;
                }
                else
                {
                    session.FindElementByAccessibilityId("decimalSeparatorButton").Click();
                    numberData = numberData.Substring(1);
                    numberLenght -= 1;
                }

            }

            if (negativeNumber != false)
            {
                session.FindElementByAccessibilityId("negateButton").Click();
            }
        }

        private bool IsElementPresent(string ElementName)
        {
            try
            {
                session.FindElementByAccessibilityId(ElementName);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsElementEnabled(string ElementName)
        {
            try
            {
                return session.FindElementByAccessibilityId(ElementName).Enabled;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public static void FinishTests()
        {
            // Close the application and delete the session
            if (session != null)
            {
                session.Quit();
                session = null;
            }

        }

    }
}
